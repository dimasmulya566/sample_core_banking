package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.MasterBankDao;
import com.test.api.ioc.Core.Banking.model.MasterBank;
import com.test.api.ioc.Core.Banking.util.CSVHelperBank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service("masterBankService")
@Slf4j
public class MasterBankServiceImpl implements MasterBankService {

    @Autowired
    private MasterBankDao masterBankDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<MasterBank> tutorials = CSVHelperBank.csvBank(file.getInputStream());
            masterBankDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<MasterBank> getAllBank() {
        return masterBankDao.findAll();
    }
}
