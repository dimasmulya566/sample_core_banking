package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.ProvinceDao;
import com.test.api.ioc.Core.Banking.model.area.Province;
import com.test.api.ioc.Core.Banking.util.CSVHelperProvinsi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class MasterProvinsiServiceImpl implements MasterProvinsiService{

    @Autowired
    private ProvinceDao provinceDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<Province> tutorials = CSVHelperProvinsi.csvProvinsi(file.getInputStream());
            provinceDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<Province> getAllProvinsi() {
        return provinceDao.findAll();
    }
}
