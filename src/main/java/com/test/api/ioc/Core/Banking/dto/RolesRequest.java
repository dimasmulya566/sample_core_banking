package com.test.api.ioc.Core.Banking.dto;

import com.test.api.ioc.Core.Banking.model.Enum.ROLE;
import lombok.Data;

@Data
public class RolesRequest {
    private ROLE name;
}
