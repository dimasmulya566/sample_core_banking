package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.area.Kelurahan;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface KelurahanService {
    void saveCsv(MultipartFile file);

    List<Kelurahan> getAllKelurahan();
}
