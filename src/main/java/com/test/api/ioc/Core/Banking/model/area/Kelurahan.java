package com.test.api.ioc.Core.Banking.model.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "MASTER_KELURAHAN")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kelurahan {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID")
    @Column(name="ID")
    @SequenceGenerator(name = "ID", sequenceName = "ISEQ$$_83796", allocationSize=1)
    @Id
    private Integer id;
    @Column(name="KELURAHAN_CODE")
    private String kelurahanCode;
    @Column(name="KELURAHAN_NAME")
    private String kelurahanName;
    @Column(name="KECAMATAN_CODE")
    private String kecamatanCode;
}
