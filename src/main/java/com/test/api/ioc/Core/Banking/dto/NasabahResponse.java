package com.test.api.ioc.Core.Banking.dto;

import com.test.api.ioc.Core.Banking.model.MasterBank;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class NasabahResponse {
    private Long id;
    private String nama;
    private String alamat;
    private String noHp;
    private String email;
    private String noRekening;
    private BigDecimal saldo;
    private String bankName;
    private String bankCode;
    private String kecamatanName;
    private String kelurahanName;
    private String cityName ;
    private String provinceName;
    private String zipCode;
}
