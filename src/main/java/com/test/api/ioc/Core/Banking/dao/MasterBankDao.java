package com.test.api.ioc.Core.Banking.dao;


import com.test.api.ioc.Core.Banking.model.MasterBank;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterBankDao extends JpaRepository<MasterBank, Long> {
    @Query("select * from master_bank where bank_code = :bank_code")
    MasterBank findByBankCode(String bank_code);
}
