package com.test.api.ioc.Core.Banking;
import com.test.api.ioc.Core.Banking.filters.AuthFilter;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class CoreBankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreBankingApplication.class, args);
	}
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize(DataSize.parse("124MB"));
		factory.setMaxRequestSize(DataSize.parse("124MB"));
		return factory.createMultipartConfig();
	}

	@Bean
	public FilterRegistrationBean<AuthFilter> loggingFilter(){
		FilterRegistrationBean<AuthFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new AuthFilter());
		registrationBean.addUrlPatterns("/api/*");

		return registrationBean;
	}


}
