package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.*;
import com.test.api.ioc.Core.Banking.dto.NasabahRequest;
import com.test.api.ioc.Core.Banking.dto.NasabahResponse;
import com.test.api.ioc.Core.Banking.dto.TransferRequest;
import com.test.api.ioc.Core.Banking.exception.EtAuthException;
import com.test.api.ioc.Core.Banking.model.MasterBank;
import com.test.api.ioc.Core.Banking.model.Nasabah;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Service("nasabahService")
@Slf4j
public class NasabahServiceImpl implements NasabahService {
    @Autowired
    private NasabahRepo nasabahRepo;
    @Autowired
    private MasterBankDao masterBankDao;
    @Autowired
    private ProvinceDao provinceDao;
    @Autowired
    private KotaDao kotaDao;
    @Autowired
    private KelurahanDao kelurahanDao;
    @Autowired
    private KecamatanDao kecamatanDao;
    @Autowired
    private ZIpcodeDao zIpcodeDao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private RegisterDao registerDao;
    @Autowired
    private ModelMapper mapper;

    public Nasabah convertToEntity(NasabahRequest request){
        return mapper.map(request, Nasabah.class);
    }

    public NasabahResponse convertToResponse(Nasabah nasabah){
        return mapper.map(nasabah, NasabahResponse.class);
    }

    @Override
    public Map<String, Object> create(Integer userCode,NasabahRequest request,Long bankId,Integer provinceId,Integer cityId,Integer kelurahanId,Integer kecamatanId,Integer zipCodeId) throws EtAuthException {
        Map<String,Object> result = new HashMap<>();
        try {
            Pattern pattern = Pattern.compile("^(.+)@(.+)$");
            int norek= (int) (Math.random() * 1000000000);
            if (request.getNama().isEmpty() || request.getNama().length() < 3) {
                result.put("message", "Nama Nasabah tidak boleh kosong dan minimal 3 karakter");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            if (request.getAlamat().isEmpty() || request.getAlamat().length() < 3) {
                result.put("message", "Alamat Nasabah tidak boleh kosong dan minimal 3 karakter");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            if (request.getNoHp().isEmpty() || request.getNoHp().length() < 10) {
                result.put("message", "No Hp Nasabah tidak boleh kosong dan minimal 10 karakter");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            var bank = masterBankDao.findById(bankId);
            var province = provinceDao.findById(provinceId);
            var city = kotaDao.findById(cityId);
            var kelurahan = kelurahanDao.findById(kelurahanId);
            var kecamatan = kecamatanDao.findById(kecamatanId);
            var zipcode = zIpcodeDao.findById(zipCodeId);
            var register = registerDao.findById(userCode);
            var roles = rolesDao.findById(register.get().getRoles().getId());
            var nasabah = convertToEntity(request);
            var email = request.getEmail();
            if(!pattern.matcher(email).matches()) {
                result.put("message", "Invalid email format");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            Integer count = nasabahRepo.getCountEmail(email);
            if(count > 0){
                result.put("message", "Email already in use");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
//                throw new EtAuthException("Email already in use");
            }
            nasabah.setBankId(bank.get());
            nasabah.setProvinceId(province.get());
            nasabah.setKotaId(city.get());
            nasabah.setKelurahanId(kelurahan.get());
            nasabah.setKecamatanId(kecamatan.get());
            nasabah.setZipcode(zipcode.get());
            if(roles.get().getName().name() != "ADMIN"){
                result.put("message", "Anda tidak memiliki akses untuk membuat nasabah");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            nasabah.setCreatedBy(roles.get().getName().name());
            nasabah.setCreatedOn(new Date());
            nasabah.setNoRekening(bank.get().getBankCode() + norek + request.getNoRekening());
            nasabah.setSaldo(BigDecimal.valueOf(100000));
            Nasabah saveNasabah = nasabahRepo.save(nasabah);
            log.info("Nasabah berhasil ditambahkan");
            result.put("status", HttpStatus.OK);
            result.put("message", "Data Berhasil Disimpan");
            result.put("data", convertToResponse(saveNasabah));
        }catch (Exception e){
            result.put("status", HttpStatus.BAD_REQUEST);
            result.put("message", "Data Gagal Disimpan");
            result.put("data", null);
        }
        return result;
    }

    @Override
    public Map<String, Object> updateNasabah(Integer userCode,NasabahRequest request, Long id){
        Map<String, Object> result = new HashMap<>();
        var nasabah = nasabahRepo.findById(id);
        var register = registerDao.findById(userCode);
        var roles = rolesDao.findById(register.get().getRoles().getId());
        int saldo = request.getSaldo().intValue();
        if(nasabah.isPresent()) {
//            var updateNasabah = convertToEntity(request);
            if (saldo < 50000){
                result.put("message", "Saldo tidak boleh kurang dari Rp. 50.000");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            nasabah.get().setSaldo(nasabah.get().getSaldo().add(request.getSaldo()));
            nasabah.get().setLastModifiedBy(roles.get().getName().name());
            nasabah.get().setLastModifiedOn(new Date());
            nasabahRepo.save(nasabah.get());
            result.put("status", HttpStatus.OK);
            result.put("message", "Data Berhasil Diupdate");
        }else{
            result.put("status", HttpStatus.BAD_REQUEST);
            result.put("message", "Data Gagal Diupdate");
            result.put("data", "Kosong");
        }
        return result;
    }

    @Transactional
    @Override
    public Map<String, Object> transfer(Integer userCode,TransferRequest request) {
        Map<String,Object> result = new HashMap<>();
        var nasabah1 = nasabahRepo.findByNorek(request.getNoRekening1(),request.getId1());
        var nasabah2 = nasabahRepo.findByNorek(request.getNoRekening2(),request.getId2());
        int chargeFee = 6500;
        if (nasabah1.getBankId() != nasabah2.getBankId()) {
//            if (nasabah1.getSaldo().intValue() < request.getSaldo().intValue()) {
//                result.put("message", "Saldo tidak boleh kurang dari jumlah transfer");
//                result.put("status", HttpStatus.BAD_REQUEST);
//                return result;
//            }
            if (nasabah1.getSaldo().intValue() < request.getSaldo().intValue() + chargeFee) {
                result.put("message", "Saldo tidak boleh kurang dari jumlah transfer + biaya admin");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            if (nasabah1.getNoRekening().equals(nasabah2.getNoRekening())) {
                result.put("message", "No Rekening tidak boleh sama");
                result.put("status", HttpStatus.BAD_REQUEST);
                return result;
            }
            nasabah1.setSaldo(nasabah1.getSaldo().subtract(request.getSaldo()));
            nasabah1.setSaldo(nasabah1.getSaldo().subtract(BigDecimal.valueOf(chargeFee)));
            nasabah2.setSaldo(nasabah2.getSaldo().add(request.getSaldo()));
            nasabahRepo.save(nasabah1);
            nasabahRepo.save(nasabah2);
            result.put("status", HttpStatus.OK);
            result.put("message", "Transfer Berhasil");
        }else {
            if (nasabah1 == null || nasabah2 == null) {
                result.put("status", "404");
                result.put("message", "Rekening tidak valid");
                return result;
            }
            if (nasabah1.getSaldo().compareTo(request.getSaldo()) < 0) {
                result.put("status", "404");
                result.put("message", "Saldo tidak mencukupi");
                return result;
            }

            nasabah1.setSaldo(nasabah1.getSaldo().subtract(request.getSaldo()));
            nasabah2.setSaldo(nasabah2.getSaldo().add(request.getSaldo()));
            nasabahRepo.save(nasabah1);
            nasabahRepo.save(nasabah2);

            result.put("status", HttpStatus.OK);
            result.put("message", "Transfer berhasil");
            return result;
        }

        result.put("status", HttpStatus.OK);
        result.put("message", "Transfer berhasil");
        return result;
    }

    @Override
    public Stream<Nasabah> getAll() {
        return nasabahRepo.findAll().stream();
    }

    @Override
    public Stream<NasabahResponse> findNoRek(String norek) {
        List<NasabahResponse> nasabahResponseList = new ArrayList<>();
        for (Nasabah nasabah : nasabahRepo.findByNorekList(norek)) {
            if (nasabah.getNoRekening().contains(norek)) {
                nasabahResponseList.add(convertToResponse(nasabah));
            }
        }
        return nasabahResponseList.stream();
    }
}
