package com.test.api.ioc.Core.Banking.controller;

import com.test.api.ioc.Core.Banking.dto.RolesRequest;
import com.test.api.ioc.Core.Banking.model.Roles;
import com.test.api.ioc.Core.Banking.service.RolesService;
import com.test.api.ioc.Core.Banking.util.ErrorParseUtility;
import com.test.api.ioc.Core.Banking.util.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.test.api.ioc.Core.Banking.constant.DataBaseConstant.messageDone;
import static com.test.api.ioc.Core.Banking.constant.DataBaseConstant.messageError;


@RestController
public class RolesController {

    @Autowired
    RolesService rolesService;

    @PostMapping(path = "/rolesCreate")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ResponseData<Object>> createData(@Valid
                                                           @RequestBody RolesRequest request, Errors errors)
    {
        ResponseData<Object> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            responseData.setStatus(false);
            responseData.setMessages(ErrorParseUtility.parse(errors));
        }
        try {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(rolesService.create(request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.getMessages().add(ex.getMessage());
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public List<Roles> findAll(){
        return rolesService.findAll();
    }
}
