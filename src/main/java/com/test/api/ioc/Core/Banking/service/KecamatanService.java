package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.area.Kecamatan;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface KecamatanService {
    void saveCsv(MultipartFile file);

    List<Kecamatan> getAllKecamatan();
}
