package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.RolesDao;
import com.test.api.ioc.Core.Banking.dto.RolesRequest;
import com.test.api.ioc.Core.Banking.dto.RolesResponse;
import com.test.api.ioc.Core.Banking.model.Roles;
import com.test.api.ioc.Core.Banking.util.ConstantMessages;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RolesServiceImpl implements RolesService {

    @Autowired
    RolesDao rolesDao;
    @Autowired
    private ModelMapper mapper;

    public Roles convertToEntity(RolesRequest request){
        return mapper.map(request, Roles.class);
    }

    public RolesResponse convertToResponse(Roles roles){
        return mapper.map(roles, RolesResponse.class);
    }
    @Override
    public Map<String, Object> create(RolesRequest request) {
        Map<String, Object> result = new HashMap<>();
        var roles = convertToEntity(request);
        Roles rolesSave = rolesDao.save(roles);
        result.put(ConstantMessages.RESPONSE_STRING, HttpStatus.OK);
        result.put(ConstantMessages.MESSAGE_STRING, ConstantMessages.SUCCESS_STRING);
        result.put(ConstantMessages.DATA_STRING, convertToResponse(rolesSave));
        return result;
    }

    @Override
    public List<Roles> findAll(){
        return rolesDao.findAll();
    }
}
