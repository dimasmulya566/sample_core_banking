package com.test.api.ioc.Core.Banking.dao;

import com.test.api.ioc.Core.Banking.exception.EtAuthException;
import com.test.api.ioc.Core.Banking.model.MasterRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RegisterDao extends JpaRepository<MasterRegister, Integer> {
    Optional<MasterRegister> findByUsername(String username);
    @Query(value = "select * from master_register where email = :email"  , nativeQuery = true)
    MasterRegister findByEmail(@Param("email") String email) throws EtAuthException;
    //mencari jumlah email yg sama
    @Query(value = "select count(*) from master_register where email = :email"   , nativeQuery = true)
    Integer getCountEmail(String email);

    @Query(value = "select count(*) from master_register where username = :username"   , nativeQuery = true)
    Integer getCountUsername(String username);


}
