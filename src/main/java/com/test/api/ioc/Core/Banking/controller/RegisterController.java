package com.test.api.ioc.Core.Banking.controller;


import com.test.api.ioc.Core.Banking.constant.DataBaseConstant;
import com.test.api.ioc.Core.Banking.dto.RegisterRequest;
import com.test.api.ioc.Core.Banking.endpoint.UserEndpoint;
import com.test.api.ioc.Core.Banking.model.MasterRegister;
import com.test.api.ioc.Core.Banking.service.RegisterService;
import com.test.api.ioc.Core.Banking.util.Constants;
import com.test.api.ioc.Core.Banking.util.ResponseData;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
 import com.test.api.ioc.Core.Banking.endpoint.UserEndpoint.*;

import static com.test.api.ioc.Core.Banking.constant.DataBaseConstant.messageDone;
import static com.test.api.ioc.Core.Banking.constant.DataBaseConstant.messageError;

@RestController
@RequestMapping(value = UserEndpoint.user)
public class RegisterController {
    @Autowired
    private RegisterService userDetailsService;

    @RequestMapping(value = UserEndpoint.registerUser, method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> register(@Valid @PathVariable("roleId") Integer roleId, @RequestBody RegisterRequest users, Errors errors) {
        Map<String,Object> map = new HashMap<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                map.put("message",error.getDefaultMessage());
            }
            map.put("status",false);
            map.put("payload",null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);

        }   else {
            map.put("status",true);
            map.put("message","success");
            map.put("payload",jwtTokenRegister(userDetailsService.register(users,roleId)));

        }
        return ResponseEntity.ok(map);
    }

    @RequestMapping(value = UserEndpoint.login, method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> login(@Valid @RequestBody Map<String,Object> objectMap, Errors errors) {
        String email = (String) objectMap.get("email");
        String password = (String) objectMap.get("password");
        Map<String,Object> map = new HashMap<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                map.put("message",error.getDefaultMessage());
            }
            map.put("status",false);
            map.put("payload",null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);

        }   else {
            map.put("status",true);
            map.put("payload",jwtTokenLogin(userDetailsService.validateUser(email,password)));

        }
        return ResponseEntity.ok(map);
    }

    private Map<String,String> jwtTokenRegister(MasterRegister masterRegister){
        long Timestamp = System.currentTimeMillis();
        String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, Constants.API_SECRET_KEY)
                .setIssuedAt(new Date(Timestamp))
                .setExpiration(new Date(Timestamp + Constants.TOKEN_VALIDITY))
                .claim("id", masterRegister.getId())
                .claim("username", masterRegister.getUsername())
                .claim("email", masterRegister.getEmail())
                .claim("password", masterRegister.getPassword())
                .claim("roleId", masterRegister.getRoles().getId())
                .compact();
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        map.put("message", DataBaseConstant.register);
        return map;
    }

    private Map<String,String> jwtTokenLogin(MasterRegister masterRegister){
        long Timestamp = System.currentTimeMillis();
        String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, Constants.API_SECRET_KEY)
                .setIssuedAt(new Date(Timestamp))
                .setExpiration(new Date(Timestamp + Constants.TOKEN_VALIDITY))
                .claim("id", masterRegister.getId())
                .claim("username", masterRegister.getUsername())
                .claim("email", masterRegister.getEmail())
                .claim("password", masterRegister.getPassword())
                .compact();
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        map.put("message", DataBaseConstant.login);
        return map;
    }

    @RequestMapping(value = UserEndpoint.updatePassword, method = RequestMethod.PUT)
    public ResponseEntity<ResponseData<Object>> updatePassword(@Valid @RequestBody RegisterRequest registerRequest, @PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId, Errors errors) {
        ResponseData<Object> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);

        }   else {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(userDetailsService.updatePassword(registerRequest,userId, roleId));

        }
        return ResponseEntity.ok(responseData);
    }
}

