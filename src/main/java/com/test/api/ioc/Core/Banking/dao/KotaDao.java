package com.test.api.ioc.Core.Banking.dao;

import com.test.api.ioc.Core.Banking.model.area.Kota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KotaDao extends JpaRepository<Kota,Integer> {
}
