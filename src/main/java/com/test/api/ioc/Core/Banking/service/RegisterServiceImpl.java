package com.test.api.ioc.Core.Banking.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.test.api.ioc.Core.Banking.dao.RegisterDao;
import com.test.api.ioc.Core.Banking.dao.RolesDao;
import com.test.api.ioc.Core.Banking.dto.RegisterRequest;
import com.test.api.ioc.Core.Banking.exception.EtAuthException;
import com.test.api.ioc.Core.Banking.model.MasterRegister;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.regex.Pattern;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private RegisterDao registerDao;

    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;



    PasswordEncoder passwordEncoder;

    private static final String FIND_BY_EMAIL = "SELECT id,username,password,email,role_id,created_date,updated_date FROM master_register WHERE email = ?";
    public MasterRegister convertToEntity(RegisterRequest request){
        return mapper.map(request, MasterRegister.class);
    }

    private RowMapper<MasterRegister> userRowMapper = ((rs, rowNum) -> {
        return new MasterRegister(
                rs.getInt("id"),
                rs.getString("username"),
                rs.getString("email"),
                rs.getString("password"),
                rs.getDate("created_date"),
                rs.getDate("updated_date")
        );
    });

    @Override
    public MasterRegister validateUser(String email, String password) throws EtAuthException {
            MasterRegister user = jdbcTemplate.queryForObject(FIND_BY_EMAIL, new Object[]{email}, userRowMapper);
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), user.getPassword());
        char[] bcryptChars = BCrypt.with(BCrypt.Version.VERSION_2A).hashToChar(6, password.toCharArray());
        String bcryptString = new String(bcryptChars);
        if (result.verified)
            if (user.getPassword().equals(bcryptString))
                    throw new EtAuthException("Invalid password");
            return user;

    }

    @Override
    public MasterRegister register(RegisterRequest reqest, Integer id) throws EtAuthException {
        // untuk meng-encrypt password ke database
        String hashPassword = org.mindrot.jbcrypt.BCrypt.hashpw(reqest.getPassword(), org.mindrot.jbcrypt.BCrypt.gensalt(10));
        // untuk mengecek format email
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        var email = reqest.getEmail();
        var username = reqest.getUsername();
        if (reqest.getEmail() != null) email = reqest.getEmail().toLowerCase();
        if(!pattern.matcher(email).matches())
            throw new EtAuthException("Invalid email format");
        if(!username.matches("^[a-zA-Z0-9]*$"))
            throw new EtAuthException("Invalid username format");
        if (!username.equals(reqest.getUsername())) throw new EtAuthException("Invalid username");
        Integer count = registerDao.getCountEmail(email);
        if(count > 0)
            throw new EtAuthException("Email already in use");
        Integer countUsername = registerDao.getCountUsername(username);
        if(countUsername > 0)
            throw new EtAuthException("Username already in use");
        var data = convertToEntity(reqest);
        var roles = rolesDao.findById(id);
        data.setPassword(hashPassword);
        data.setCreateDate(new Date());
        data.setRoles(roles.get());
        MasterRegister userId = registerDao.save(data);
        return userId;
    }

    @Override
    public MasterRegister updatePassword(RegisterRequest request, Integer registerId, Integer id ) throws EtAuthException {
        String hashPassword = org.mindrot.jbcrypt.BCrypt.hashpw(request.getPassword(), org.mindrot.jbcrypt.BCrypt.gensalt(10));
        var users = registerDao.findById(registerId);
        var roles = rolesDao.findById(id);
        users.get().setPassword(hashPassword);
        users.get().setEmail(request.getEmail());
        users.get().setUsername(request.getUsername());
        users.get().setRoles(roles.get());
        users.get().setUpdateDate(new Date());
        MasterRegister userId = registerDao.save(users.get());
        return userId;
    }

}
