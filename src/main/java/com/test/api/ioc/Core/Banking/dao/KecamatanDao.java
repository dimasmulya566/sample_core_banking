package com.test.api.ioc.Core.Banking.dao;


import com.test.api.ioc.Core.Banking.model.area.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KecamatanDao extends JpaRepository<Kecamatan,Integer> {
}
