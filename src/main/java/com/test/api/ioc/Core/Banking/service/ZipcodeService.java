package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.area.Zipcode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ZipcodeService {
    void saveCsv(MultipartFile file);

    List<Zipcode> getAllZipcode();
}
