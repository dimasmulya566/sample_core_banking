package com.test.api.ioc.Core.Banking.util;

import com.test.api.ioc.Core.Banking.model.MasterBank;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelperBank {
    private static final String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<MasterBank> csvBank(InputStream io) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(io,"UTF-8"));
            CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            List<MasterBank> bankList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                MasterBank masterBank = new MasterBank();
                masterBank.setId(Long.valueOf(csvRecord.get("ID")));
                masterBank.setBankCode(csvRecord.get("BANK_CODE"));
                masterBank.setBankName(csvRecord.get("BANK_NAME"));
                masterBank.setIsActive(csvRecord.get("IS_ACTIVE"));
                bankList.add(masterBank);
            }
            csvParser.close();
            return bankList;
        } catch (Exception e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
