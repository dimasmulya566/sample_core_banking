package com.test.api.ioc.Core.Banking.dao;

import com.test.api.ioc.Core.Banking.model.Enum.ROLE;
import com.test.api.ioc.Core.Banking.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RolesDao extends JpaRepository<Roles,Integer> {
    Optional<Roles> findByName(ROLE name);

    @Query(value = "SELECT * FROM roles WHERE id = :id",nativeQuery = true)
    Optional<Roles> findByIdRole(Integer id);
}
