package com.test.api.ioc.Core.Banking.util;

public interface ConstantMessages {
    final static String MESSAGE_STRING = "message";
    final static String DATA_STRING = "data";
    final static String RESPONSE_STRING = "response";
    final static String TOTAL_STRING = "total";
    final static String SUCCESS_STRING = "Success";
    final static String EMPTY_DATA_STRING = "Data Kosong";
    final static String ID_NOT_FOUND = "Id Tidak Ditemukan";

}
