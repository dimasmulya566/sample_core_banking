package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.dto.RolesRequest;
import com.test.api.ioc.Core.Banking.model.Roles;

import java.util.List;
import java.util.Map;

public interface RolesService {
    Map<String, Object> create(RolesRequest request);
    List<Roles> findAll();
}
