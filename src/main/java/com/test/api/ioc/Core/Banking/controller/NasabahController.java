package com.test.api.ioc.Core.Banking.controller;

import com.test.api.ioc.Core.Banking.dto.NasabahRequest;
import com.test.api.ioc.Core.Banking.dto.NasabahResponse;
import com.test.api.ioc.Core.Banking.dto.TransferRequest;
import com.test.api.ioc.Core.Banking.model.Nasabah;
import com.test.api.ioc.Core.Banking.service.NasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/nasabah")
public class NasabahController {

    @Autowired
    private NasabahService service;

    @RequestMapping(value = "/create/{id}",method = RequestMethod.POST)
    public Map<String, Object> create(HttpServletRequest httpServletRequest, @RequestBody NasabahRequest request, @PathVariable("id") Long id,
                                      @RequestParam("provinceId") Integer provinceId,
                                      @RequestParam("cityId") Integer cityId,
                                      @RequestParam("kelurahanId") Integer kelurahanId,
                                      @RequestParam("kecamatanId") Integer kecamatanId,
                                      @RequestParam("zipCodeId") Integer zipCodeId) throws Exception {
        int page = (Integer) httpServletRequest.getAttribute("id");

        return service.create(page,request,id,provinceId,cityId,kelurahanId,kecamatanId,zipCodeId);
    }

    @RequestMapping(value = "/update/{id}",method = RequestMethod.PUT)
    public Map<String,Object> update(HttpServletRequest httpServletRequest,@RequestBody NasabahRequest request, @PathVariable("id") Long id){
        int page = (Integer) httpServletRequest.getAttribute("id");
        return service.updateNasabah(page,request,id);
    }

    @RequestMapping(value = "/transfer",method = RequestMethod.POST)
    public Map<String,Object> transfer(HttpServletRequest httpServletRequest,@RequestBody TransferRequest request){
        int page = (Integer) httpServletRequest.getAttribute("id");
        return service.transfer(page,request);
    }

    @RequestMapping(value = "/getall",method = RequestMethod.GET)
    public Stream<Nasabah> getAll(){
        return service.getAll();
    }

    @RequestMapping(value = "/findnorek/{norek}",method = RequestMethod.GET)
    public Stream<NasabahResponse> findNoRek(@PathVariable("norek") String norek){
        return service.findNoRek(norek);
    }
}

