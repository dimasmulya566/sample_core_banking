package com.test.api.ioc.Core.Banking.model.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "MASTER_KECAMATAN")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kecamatan {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID")
    @Column(name="ID")
    @SequenceGenerator(name = "ID", sequenceName = "ISEQ$$_83794", allocationSize=1)
    @Id
    private Integer id;
    @Column(name="KECAMATAN_CODE")
    private String kecamatanCode;
    @Column(name="KECAMATAN_NAME")
    private String kecamatanName;
    @Column(name="CITY_CODE")
    private String cityCode;
}
