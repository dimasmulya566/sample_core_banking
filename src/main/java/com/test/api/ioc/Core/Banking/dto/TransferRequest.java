package com.test.api.ioc.Core.Banking.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TransferRequest {
    private Long id1;
    private Long id2;
    private String nama1;
    private String nama2;
    private String noRekening1;
    private String noRekening2;
    private BigDecimal saldo;
    private String description;

}
