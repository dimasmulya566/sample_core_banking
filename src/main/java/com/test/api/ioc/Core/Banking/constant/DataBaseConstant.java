package com.test.api.ioc.Core.Banking.constant;

public class DataBaseConstant {
    public static final String messageDone = "Berhasil di input ke database";

    public static final String login = "Login Successfuly";
    public static final String register = "Register Successfuly";
    public static final String messageError = "Terjadi Kesalahan! Silahkan dicek kembali";
    public static final String messageUpdate = "Berhasil Di Update Datanya";
    public static final String messageErrorUpdate = "Terjadi Kesalahan! Saat Update";
}
