package com.test.api.ioc.Core.Banking.util;


import com.test.api.ioc.Core.Banking.model.area.Zipcode;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelperZipcode {
    private static final String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<Zipcode> csvZipcode(InputStream io) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(io,"UTF-8"));
            CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            List<Zipcode> zipcodeList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Zipcode zipcode = new Zipcode();
                zipcode.setZipCode(Integer.valueOf(csvRecord.get("ZIPCODE_CODE")));
                zipcode.setZipCodeDesc(csvRecord.get("ZIPCODE_DESC"));
                zipcode.setKelurahanCode(csvRecord.get("KELURAHAN_CODE"));
                zipcode.setKecamatanCode(csvRecord.get("KECAMATAN_CODE"));
                zipcode.setCityCode(csvRecord.get("CITY_CODE"));
                zipcode.setProvinceCode(csvRecord.get("PROVINCE_CODE"));
//                zipcode.setCreatedBy(csvRecord.get("CREATED_BY"));
//                zipcode.setCreatedDate(csvRecord.get("CREATED_DATE"));
//                zipcode.setUpdatedBy(csvRecord.get("UPDATED_BY"));
//                zipcode.setUpdatedDate(csvRecord.get("UPDATED_DATE"));
//                zipcode.setDeletedBy(csvRecord.get("DELETED_BY"));
//                zipcode.setDeletedDate(csvRecord.get("DELETED_DATE"));
                zipcodeList.add(zipcode);
            }
            csvParser.close();
            return zipcodeList;
        } catch (Exception e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
