package com.test.api.ioc.Core.Banking.dao;

import com.test.api.ioc.Core.Banking.dto.NasabahResponse;
import com.test.api.ioc.Core.Banking.model.Nasabah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public interface NasabahRepo extends JpaRepository<Nasabah, Long> {
    @Query(value = "select count(*) from nasabah where email = :email"   , nativeQuery = true)
    Integer getCountEmail(String email);

    @Query(value = "select * from nasabah where no_rekening = :noRekening and bank_id = :bank_id"   , nativeQuery = true)
    Nasabah findByNorek(String noRekening,Long bank_id);

    @Query(value = "select * from nasabah where no_rekening = :noRekening"   , nativeQuery = true)
    List<Nasabah> findByNorekList(String noRekening);

    @Query(value = "select * from nasabah where bank_id = :bank_id"   , nativeQuery = true)
    Nasabah findByBankId(Long bank_id);

}
