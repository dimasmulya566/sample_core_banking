package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.area.Province;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MasterProvinsiService {
    List<Province> getAllProvinsi();

    void saveCsv(MultipartFile file);
}
