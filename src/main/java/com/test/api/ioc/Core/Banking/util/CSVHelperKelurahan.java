package com.test.api.ioc.Core.Banking.util;


import com.test.api.ioc.Core.Banking.model.area.Kelurahan;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelperKelurahan {
    private static final String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<Kelurahan> csvKelurahan(InputStream io) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(io,"UTF-8"));
            CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            List<Kelurahan> kelurahanList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Kelurahan kelurahan = new Kelurahan();
                kelurahan.setId(Integer.parseInt(csvRecord.get("ID")));
                kelurahan.setKelurahanCode(csvRecord.get("KELURAHAN_CODE"));
                kelurahan.setKelurahanName(csvRecord.get("KELURAHAN_NAME"));
                kelurahan.setKecamatanCode(csvRecord.get("KECAMATAN_CODE"));
                kelurahanList.add(kelurahan);
            }
            csvParser.close();
            return kelurahanList;
        } catch (Exception e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
