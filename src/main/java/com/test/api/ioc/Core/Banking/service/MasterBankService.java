package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.MasterBank;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MasterBankService {
    List<MasterBank> getAllBank();
    void saveCsv(MultipartFile file);
}
