package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.model.area.Kota;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface KotaService {
    void saveCsv(MultipartFile file);

    List<Kota> getAllKota();
}
