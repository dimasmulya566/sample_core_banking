package com.test.api.ioc.Core.Banking.model;

import com.test.api.ioc.Core.Banking.model.Entity.BaseEntity;
import com.test.api.ioc.Core.Banking.model.area.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.compress.archivers.zip.Zip64Mode;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "nasabah")
@AllArgsConstructor
@NoArgsConstructor
public class Nasabah extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nama", nullable = false, length = 100)
    private String nama;
    @Column(name = "alamat", nullable = false, length = 100)
    private String alamat;
    @Column(name = "no_telp", nullable = false, length = 100)
    private String noHp;
    @Column(name = "email", nullable = false, length = 100)
    private String email;
    @Column(name = "no_rekening", nullable = false, length = 100)
    private String noRekening;
    @Column(name = "saldo")
    private BigDecimal saldo;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = MasterBank.class)
    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    private MasterBank bankId;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Province.class)
    @JoinColumn(name = "provinsi_id", referencedColumnName = "id")
    private Province provinceId;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kota.class)
    @JoinColumn(name = "kota_id", referencedColumnName = "id")
    private Kota kotaId;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kecamatan.class)
    @JoinColumn(name = "kecamatan_id", referencedColumnName = "id")
    private Kecamatan kecamatanId;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kelurahan.class)
    @JoinColumn(name = "kelurahan_id", referencedColumnName = "id")
    private Kelurahan kelurahanId;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Zipcode.class)
    @JoinColumn(name = "zipcode", referencedColumnName = "ZIPCODE_CODE")
    private Zipcode zipcode;

}
