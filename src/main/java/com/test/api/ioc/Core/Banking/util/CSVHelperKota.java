package com.test.api.ioc.Core.Banking.util;


import com.test.api.ioc.Core.Banking.model.area.Kota;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelperKota {
    private static final String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<Kota> csvCity(InputStream io) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(io,"UTF-8"));
            CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            List<Kota> provincesList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Kota city = new Kota();
                city.setId(Integer.parseInt(csvRecord.get("ID")));
                city.setCityCode(csvRecord.get("CITY_CODE"));
                city.setCityName(csvRecord.get("CITY_NAME"));
                city.setProvinceCode(csvRecord.get("PROVINSI_CODE"));
                city.setDati2(csvRecord.get("DATI2"));
                provincesList.add(city);
            }
            csvParser.close();
            return provincesList;
        } catch (Exception e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
