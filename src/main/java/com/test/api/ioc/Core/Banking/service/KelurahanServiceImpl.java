package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.KelurahanDao;
import com.test.api.ioc.Core.Banking.model.area.Kelurahan;
import com.test.api.ioc.Core.Banking.util.CSVHelperKelurahan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class KelurahanServiceImpl implements KelurahanService {

    @Autowired
    private KelurahanDao kelurahanDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<Kelurahan> tutorials = CSVHelperKelurahan.csvKelurahan(file.getInputStream());
            kelurahanDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<Kelurahan> getAllKelurahan() {
        return kelurahanDao.findAll();
    }
}
