package com.test.api.ioc.Core.Banking.model.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "MASTER_PROVINSI")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Province {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID")
    @Column(name="ID")
    @SequenceGenerator(name = "ID", sequenceName = "ISEQ$$_81733", allocationSize=1)
    @Id
    private Integer id;
    @Column(name="PROVINSI_CODE")
    private String provinceCode;
    @Column(name="PROVINSI_NAME")
    private String provinceName;


}
