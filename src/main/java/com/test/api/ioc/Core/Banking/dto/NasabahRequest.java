package com.test.api.ioc.Core.Banking.dto;

import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
public class NasabahRequest {
    private String nama;
    private String alamat;
    private String noHp;
    private String email;
    private String noRekening;
    private BigDecimal saldo;

}
