package com.test.api.ioc.Core.Banking.model.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "MASTER_CITY")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kota {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID")
    @Column(name="ID")
    @SequenceGenerator(name = "ID", sequenceName = "ISEQ$$_81735", allocationSize=1)
    @Id
    private Integer id;
    @Column(name="CITY_CODE")
    private String cityCode;
    @Column(name="CITY_NAME")
    private String cityName;
    @Column(name="PROVINSI_CODE")
    private String provinceCode;
    @Column(name="DATI2")
    private String dati2;
}
