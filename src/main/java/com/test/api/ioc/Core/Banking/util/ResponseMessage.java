package com.test.api.ioc.Core.Banking.util;

import lombok.Data;

@Data
public class ResponseMessage {
    private String message;
    public ResponseMessage(String message) {
        this.message = message;
    }
}
