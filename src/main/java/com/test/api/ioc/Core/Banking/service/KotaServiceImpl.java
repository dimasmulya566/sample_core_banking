package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.KotaDao;
import com.test.api.ioc.Core.Banking.model.area.Kota;
import com.test.api.ioc.Core.Banking.util.CSVHelperKota;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class KotaServiceImpl implements KotaService {

    @Autowired
    private KotaDao kotaDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<Kota> tutorials = CSVHelperKota.csvCity(file.getInputStream());
            kotaDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<Kota> getAllKota() {
        return kotaDao.findAll();
    }
}
