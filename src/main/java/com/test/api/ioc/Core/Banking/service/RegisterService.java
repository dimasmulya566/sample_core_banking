package com.test.api.ioc.Core.Banking.service;


import com.test.api.ioc.Core.Banking.dto.RegisterRequest;
import com.test.api.ioc.Core.Banking.exception.EtAuthException;
import com.test.api.ioc.Core.Banking.model.MasterRegister;

public interface RegisterService {

    MasterRegister validateUser(String email, String password) throws EtAuthException;
    MasterRegister register(RegisterRequest reqest, Integer id) throws EtAuthException;
    public MasterRegister updatePassword(RegisterRequest request, Integer registerId, Integer id) throws EtAuthException;
}
