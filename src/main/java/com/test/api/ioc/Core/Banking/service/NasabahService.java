package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.NasabahRepo;
import com.test.api.ioc.Core.Banking.dto.NasabahRequest;
import com.test.api.ioc.Core.Banking.dto.NasabahResponse;
import com.test.api.ioc.Core.Banking.dto.TransferRequest;
import com.test.api.ioc.Core.Banking.exception.EtAuthException;
import com.test.api.ioc.Core.Banking.model.Nasabah;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Stream;

public interface NasabahService {
    Map<String, Object> create(Integer userCode,NasabahRequest request,Long bankId,Integer provinceId,Integer cityId,Integer kelurahanId,Integer kecamatanId,Integer zipCodeId) throws EtAuthException;
    Map<String, Object> updateNasabah(Integer userCode,NasabahRequest request, Long id);
    Map<String, Object> transfer(Integer userCode,TransferRequest request);
    Stream<Nasabah> getAll();
    Stream<NasabahResponse> findNoRek(String norek);

}
