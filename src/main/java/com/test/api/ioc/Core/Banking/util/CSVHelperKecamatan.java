package com.test.api.ioc.Core.Banking.util;

import com.test.api.ioc.Core.Banking.model.area.Kecamatan;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVHelperKecamatan {
    private static final String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<Kecamatan> csvKecamatan(InputStream io) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(io,"UTF-8"));
            CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            List<Kecamatan> kecamatanList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Kecamatan kecamatan = new Kecamatan();
                kecamatan.setId(Integer.parseInt(csvRecord.get("ID")));
                kecamatan.setKecamatanCode(csvRecord.get("KECAMATAN_CODE"));
                kecamatan.setKecamatanName(csvRecord.get("KECAMATAN_NAME"));
                kecamatan.setCityCode(csvRecord.get("CITY_CODE"));
                kecamatanList.add(kecamatan);
            }
            csvParser.close();
            return kecamatanList;
        } catch (Exception e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
