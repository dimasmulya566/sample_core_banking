package com.test.api.ioc.Core.Banking.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(	name = "MASTER_REGISTER",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
@Data
@NoArgsConstructor
public class MasterRegister {
    private static final long OTP_VALID_DURATION = 5 * 60 * 1000;   // 5 minutes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(name = "username")
    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @OneToOne(cascade = CascadeType.ALL,targetEntity = Roles.class)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private Roles roles;

    @NotBlank
    @Size(max = 120)
    private String password;

    @Column(name = "Created_Date", columnDefinition = "DATE")
    @CreatedDate
    private Date createDate;

    @Column(name = "Updated_Date", columnDefinition = "DATE")
    @CreatedDate
    private Date updateDate;
    public MasterRegister(Integer id, String username, String email, String password, Date createDate, Date updateDate) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }


}
