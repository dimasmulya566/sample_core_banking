package com.test.api.ioc.Core.Banking.model.area;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "Zipcode")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Zipcode {
    @Id
    @Column(name="ZIPCODE_CODE")
    private Integer zipCode;
    @Column(name="ZIPCODE_DESC")
    private String zipCodeDesc;
    @Column(name="KELURAHAN_CODE")
    private String kelurahanCode;
    @Column(name="KECAMATAN_CODE")
    private String kecamatanCode;
    @Column(name="CITY_CODE")
    private String cityCode;
    @Column(name="PROVINCE_CODE")
    private String provinceCode;
//    @Column(name = "CREATED_BY")
//    @CreatedBy
//    private String createdBy;
//
//    @Column(name = "CREATED_DATE", columnDefinition = "DATE")
//    @CreatedDate
//    private Date createdDate;
//
//    @Column(name ="UPDATED_BY")
//    @CreatedBy
//    private String updatedBy;
//
//    @Column(name = "UPDATED_DATE", columnDefinition = "DATE")
//    @CreatedDate
//    private Date updatedDate;
//
//    @Column(name ="DELETED_BY")
//    @CreatedBy
//    private String deletedBy;
//
//    @Column(name = "DELETED_DATE", columnDefinition = "DATE")
//    @CreatedDate
//    private Date deletedDate;
}
