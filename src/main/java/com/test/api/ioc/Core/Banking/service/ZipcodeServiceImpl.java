package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.ZIpcodeDao;
import com.test.api.ioc.Core.Banking.model.area.Zipcode;
import com.test.api.ioc.Core.Banking.util.CSVHelperZipcode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ZipcodeServiceImpl implements ZipcodeService {

    @Autowired
    private ZIpcodeDao zIpcodeDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<Zipcode> tutorials = CSVHelperZipcode.csvZipcode(file.getInputStream());
            zIpcodeDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<Zipcode> getAllZipcode() {
        return zIpcodeDao.findAll();
    }
}
