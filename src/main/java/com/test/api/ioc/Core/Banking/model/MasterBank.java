package com.test.api.ioc.Core.Banking.model;

import com.test.api.ioc.Core.Banking.model.Entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "MASTER_BANK")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MasterBank extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;
    @Column(name="BANK_CODE")
    private String bankCode;
    @Column(name="BANK_NAME")
    private String bankName;
    @Column(name="IS_ACTIVE")
    private String isActive;
}
