package com.test.api.ioc.Core.Banking.service;

import com.test.api.ioc.Core.Banking.dao.KecamatanDao;
import com.test.api.ioc.Core.Banking.model.area.Kecamatan;
import com.test.api.ioc.Core.Banking.util.CSVHelperKecamatan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class KecamatanServiceImpl implements KecamatanService {

    @Autowired
    private KecamatanDao kecamatanDao;

    @Override
    public void saveCsv(MultipartFile file) {
        try {
            List<Kecamatan> tutorials = CSVHelperKecamatan.csvKecamatan(file.getInputStream());
            kecamatanDao.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    @Override
    public List<Kecamatan> getAllKecamatan(){
        return kecamatanDao.findAll();
    }
}
