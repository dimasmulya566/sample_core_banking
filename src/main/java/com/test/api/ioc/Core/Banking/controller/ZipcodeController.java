package com.test.api.ioc.Core.Banking.controller;

import com.test.api.ioc.Core.Banking.model.area.Zipcode;
import com.test.api.ioc.Core.Banking.service.ZipcodeService;
import com.test.api.ioc.Core.Banking.util.CSVHelperKecamatan;
import com.test.api.ioc.Core.Banking.util.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/zipcode")
public class ZipcodeController {
    @Autowired
    private ZipcodeService zipcodeService;

    @GetMapping("/getAllZipcode")
    public ResponseEntity<List<Zipcode>> getAllZipcode() {
        try {
            List<Zipcode> tutorials = zipcodeService.getAllZipcode();

            if (tutorials.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/import-csv", method = RequestMethod.POST)
    public ResponseEntity<?> importCsvFile(@RequestParam("file") MultipartFile files) throws IOException {
        ResponseData responseData = new ResponseData<>();
       if (!CSVHelperKecamatan.hasCSVFormat(files)) {
           responseData.setMessageValidation("Please upload a csv file!");
              return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

       try {
              zipcodeService.saveCsv(files);
           responseData.setStatus(true);
              responseData.setMessageValidation("Uploaded the file successfully: " + files.getOriginalFilename());
              return ResponseEntity.status(HttpStatus.OK).body(responseData);
         } catch (Exception e) {
           responseData.setStatus(false);
              responseData.setMessageValidation("Could not upload the file: " + files.getOriginalFilename() + "!");
              return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(responseData);
       }

    }
}
