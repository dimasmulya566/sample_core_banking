package com.test.api.ioc.Core.Banking.endpoint;

public class UserEndpoint {
    public static final String user = "/user";
    public static final String roleId = "roleId";
    public static final String userId = "userId";
    public static final String emailId = "emailId";

    public static final String registerUser = "/register/{" + roleId + "}";

    public static final String login = "/login";

    public static final String updatePassword = "/updatePassword/{"+roleId+"}/{"+userId+"}";

    public static final String userCreate = "/userCreate/{" + emailId + "}";
}
