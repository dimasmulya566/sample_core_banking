package com.test.api.ioc.Core.Banking.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TransferResponse {
    private String noRekening1;
    private String noRekening2;
    private BigDecimal saldo;
    private LocalDateTime date;
    private String description;
}
